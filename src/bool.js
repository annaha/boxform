// var x = 0
// var speed = 3
var szin = {
  r : 0,
  g : 0,
  b : 0
}
var kockaHeight = 50
let grid
// let cam
function preload() {
  grid = loadImage("media/griddi.png")
}

function setup(){
  createCanvas(window.innerWidth, window.innerHeight, WEBGL)
  // cam = createCapture(VIDEO
  //   cam.hide
}

function draw(){
  background(szin.r, szin.g, szin.b)

  if (mouseY > innerHeight / 2){
    szin.r = random(0, 255)
    szin.g = random(0, 250)
    szin.b = random(0, 255)

  }
  // stroke(255)
  // strokeWeight(4)
  // noFill()
  //
  // if (mouseX > 300){
  //   ellipse(300, 200, 100, 100)
  // } else {
  //   rect(300, 200, 100, 100)
  // }
  let dx = mouseX - width / 2
  let dy = mouseY - height / 2
  let v = createVector(dx, dy, 0)
  v.div(100)
  noStroke()

  directionalLight(255, 255, 0, dx, dy, v)
  // pointLight(0, 0, 255, 0, - 900, 0)
  // pointLight(150, 0, 0, 0, 900, 0)
  // specularMaterial(255)
  //ambientMaterial(255)
  // fill("#ff6098")
  texture(grid)
  rotateX(frameCount * 0.01)
  rotateY(frameCount * 0.01)
  box(200, 200, kockaHeight)
  kockaHeight = kockaHeight + 2
}
//
// if (x > width || x < 0){
//   speed = speed * -1
// }
// ellipse(x, 200, 100, 100)
// x = x + speed
